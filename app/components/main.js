var app = angular.module('app', [
    'iaf',
])

app.controller("myCtrl", function ($scope, $rootScope, irisApi, irisGeolocation, irisNotification, irisCollectionService, irisUploadService, irisAuthService, $fancyModal) {

    $scope.signup = { username: null, email: null, password: null };
    $scope.login = { username: null, password: null };
    $scope.userAuthenticated = () => irisAuthService.userAuthenticated();

    this.$onInit = function () {

    }

    $scope.getAll = (model) => {
        irisApi.listDocuments(model)
            .then(success => {
                $scope.documents = success.data;
            });
    }

    $scope.addToCollection = function () {
        irisApi.getById('events', "5ad4fa23038c0c0014d1ea57")
            .then(success => {
                let data = success.data;
                irisCollectionService.add($scope, data);
            })
    }

    $scope.openUploader = () => {
        // irisUploadService.open();
        irisUploadService.open({ format: 'image, video, audio, document', purpose: 'third-party-upload' })
    }

    $scope.login = (credentials) => {
        irisAuthService.login(credentials)
            .then(res => {
                console.log(res)
                window.location.reload();
            })
            .catch(err => {
                console.log(err)
                irisAuthService.defaultLogin();
            })
        
        // .then(res => console.log(res))
        // .catch(err => console.error(err));
    }

    $scope.signup = (credentials) => {
        irisAuthService.signup(credentials)
            .then(res => console.log(res))
            .catch(err => console.error(err));
    }

    $scope.logout = () => irisAuthService.clearCredentials();

    $rootScope.pref_doc ? $rootScope.pref_doc.then(data => $scope.appPrefDocument = data) : null;
});